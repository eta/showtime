#![allow(deprecated)] // want to use sleep_ms, don't care

use std::net::{UdpSocket, Ipv4Addr, SocketAddr};
#[cfg(feature = "tcp")]
use std::net::TcpListener;
use byteorder::{BigEndian, WriteBytesExt};
#[cfg(any(feature = "jack", test))]
use byteorder::ByteOrder;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, AtomicU8, Ordering};
use std::time::Instant;
use std::sync::mpsc;
use std::io::{Write};
#[cfg(feature = "stdin")]
use std::io::{self, BufReader};
#[cfg(any(feature = "stdin", feature = "tcp"))]
use std::io::Read;
use bounded_spsc_queue::{Consumer, Producer, make};
use std::thread::Builder;
#[cfg(feature = "jack")]
use sqa_jack::{JackHandler, JackPort, JackCallbackContext, JackControl, JackConnection, PORT_IS_INPUT, JackNFrames};
#[cfg(feature = "jack")]
use samplerate::{Samplerate, ConverterType};

const BUFFER_SIZE: usize = 992*512;

#[cfg(feature = "jack")]
struct ShowtimeJackHandler {
    left: JackPort,
    right: JackPort,
    tx: Producer<f32>
}

#[cfg(feature = "jack")]
impl JackHandler for ShowtimeJackHandler {
    fn process(&mut self, ctx: &JackCallbackContext) -> JackControl {
        let lbuf = ctx.get_port_buffer(&self.left).unwrap();
        let rbuf = ctx.get_port_buffer(&self.right).unwrap();
        for (l, r) in lbuf.iter().zip(rbuf.iter()) {
            self.tx.try_push(*l);
            self.tx.try_push(*r);
        }
        JackControl::Continue
    }
    fn sample_rate(&mut self, rate: JackNFrames) -> JackControl {
        println!("sample rate: {}", rate);
        self.tx.push(rate as _);
        JackControl::Continue
    }
    fn xrun(&mut self) -> JackControl {
        eprintln!("JACK Xrun!");
        JackControl::Continue
    }
}

#[cfg(feature = "jack")]
fn resampler_loop(from_jack: Consumer<f32>, to_feeder: mpsc::SyncSender<Vec<f32>>) {
    let sample_rate: u32;
    loop {
        let sample_f32 = from_jack.pop();
        if sample_f32 > 1.0 {
            // it's the sample rate!
            sample_rate = sample_f32 as _;
            println!("got sample rate");
            break;
        }
    }
    let resampler = Samplerate::new(ConverterType::SincBestQuality, sample_rate, 44100, 2).unwrap();
    println!("resampling from {} Hz -> {} Hz", sample_rate, 44100);
    const SAMPLES_PER_BUF: usize = 512;
    let mut buf: Vec<f32> = Vec::with_capacity(SAMPLES_PER_BUF);
    for _ in 0..SAMPLES_PER_BUF {
        buf.push(0.0);
    }
    let mut ptr = 0;
    loop {
        buf[ptr] = match from_jack.try_pop() {
            Some(smpl) => smpl,
            None => {
                ::std::thread::sleep_ms(1);
                continue;
            }
        };
        ptr += 1;
        if ptr == SAMPLES_PER_BUF {
            let resampled = resampler.process(&buf).unwrap();
            to_feeder.send(resampled.clone()).unwrap();
            ptr = 0;
        }
    }
}

#[cfg(feature = "jack")]
fn jack_feeder_loop(from_jack: mpsc::Receiver<Vec<f32>>, out: mpsc::SyncSender<Vec<u8>>) {
    loop {
        let mut buf = Vec::with_capacity(BUFFER_SIZE / 2);
        for _ in 0..(BUFFER_SIZE / 2) {
            buf.write_all(&[0]).unwrap();
        }
        let mut read_bytes = 0;
        loop {
            let samples_f32 = from_jack.recv().unwrap();
            for sample_f32 in samples_f32 {
                let sample_i32 = ((i32::MAX as f32) * sample_f32) as i32;
                BigEndian::write_i32(&mut buf[read_bytes..], sample_i32);
                read_bytes += 4;
                if read_bytes == (BUFFER_SIZE / 2) {
                    if let Err(e) = out.try_send(buf.clone()) {
                        eprintln!("send failed: {}", e);
                    }
                    read_bytes = 0;
                }
            }
        }
    }
}

struct FrameChunker<'a> {
    inner: &'a [u8],
    frame_no: u16,
    seq_no: u16
}

impl<'a> FrameChunker<'a> {
    fn new(frame_no: u16, inner: &'a [u8]) -> Self {
        Self {
            inner,
            frame_no,
            seq_no: 0
        }
    }
    fn next_chunk(&mut self) -> Vec<u8> {
        let mut ret = Vec::with_capacity(1024);
        let (remaining, last) = if self.inner.len() > 1020 {
            (1020, false)
        }
        else {
            (self.inner.len(), true)
        };
        ret.write_u16::<BigEndian>(self.frame_no).unwrap();
        ret.write_u16::<BigEndian>(self.seq_no).unwrap();
        if last {
            ret[2] |= 0b1000_0000;
        }
        ret.extend(&self.inner[..remaining]);
        self.inner = &self.inner[remaining..];
        self.seq_no += 1;
        ret
    }
    fn is_empty(&self) -> bool {
        self.inner.is_empty()
    }
}

const MAGIC_ADDRESS: Ipv4Addr = Ipv4Addr::new(226, 2, 2, 2);

fn make_vsync(frame_no: u16) -> Vec<u8> {
    let mut ret = Vec::with_capacity(20);
    ret.write_all(&[0, 0, 0, 0]).unwrap();
    ret.write_u16::<BigEndian>(frame_no).unwrap();
    for _ in 0..14 {
        ret.write_all(&[0]).unwrap();
    }
    ret
}

fn audio_thread(udp: UdpSocket, dest: SocketAddr, queue: Consumer<u8>, fc: Arc<AtomicU8>) {
    let mut pkt = [0u8; 1008];
    // Pre-write the header on to the packet.
    for (i, b) in [0, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0x55, 0, 0, 0].iter().enumerate() {
        pkt[i] = *b;
    }
    let now = Instant::now();
    // Monotonic nanosecond counter.
    let cur_nanos = || {
        Instant::now().duration_since(now).as_micros()
    };
    const INTERVAL: u128 = 2811;
    // Next time (on the counter) we need to send a packet.
    let mut next = INTERVAL;
    // Current buffer pointer.
    let mut ptr = 16;
    let mut ldiff = 0;
    // Consecutive Xrun counter.
    let mut num_xruns = 0;
    loop {
        let cur = cur_nanos();
        if cur >= next {
            ldiff = cur - next;
            // Send the packet.
            udp.send_to(&pkt, &dest).unwrap();
            // Check if we managed to fill the buf; if not, we've xrun'd.
            if ptr != 1008 {
                num_xruns += 1;
                fc.store(0, Ordering::Relaxed);
                if ptr != 16 { // reduce spam
                    eprintln!("/!\\ XRUN ptr={}smpls, t={}ns", ptr, cur);
                }
            }
            else {
                // Reset the pointer.
                ptr = 16;
                num_xruns = 0;
                fc.store(1, Ordering::Relaxed);
            }
            // Check if we're already going to miss the next deadline; if so, warn.
            if cur >= (next + INTERVAL) {
                eprintln!("/!\\ WARNING WARNING, {}ns behind!", cur - next);
                fc.store(2, Ordering::Relaxed);
            }
            // Advance the deadline so it's 2.8ms after the previous one.
            // We skip over if we're already going to miss it.
            while cur >= next {
                next += INTERVAL;
            }
        }
        // Okay, now time to fill the buffer a bit.
        if ptr < 1008 {
            if num_xruns > 5 {
                // Let's just fill it up with silence instead.
                // (otherwise you get a buzzing effect that's highly irritating)
                pkt[ptr] = 0;
                ptr += 1;
            }
            else {
                if let Some(smpl) = queue.try_pop() {
                    pkt[ptr] = smpl;
                    ptr += 1;
                }
            }
        }
        else {
            if ldiff > 300 {
                eprintln!("! wakeup delayed {}ns", ldiff);
                ldiff = 0;
            }
            #[cfg(not(feature = "noyield"))]
            ::std::thread::yield_now();
        }
    }
}

fn make_heartbeat(seq_no: u16, cur_ts: u16, init: bool) -> Vec<u8> {
    let mut ret = Vec::with_capacity(512);
    ret.write_all(&[0x54, 0x46, 0x36, 0x7a, 0x63, 0x01, 0x00]).unwrap();
    ret.write_u16::<BigEndian>(seq_no).unwrap();
    ret.write_all(&[0x00, 0x00, 0x03, 0x03, 0x03, 0x00, 0x24, 0x00, 0x00]).unwrap();
    for _ in 0..8 {
        ret.write_all(&[0]).unwrap();
    }
    let (mode, a, b, c, d, e, f, other) = if init {
        (3, 1920, 1080, 599, 1920, 1080, 120, 3)
    }
    else {
        (16, 0, 0, 0, 0, 0, 120, 0)
    };
    for thing in &[mode, a, b, c, d, e, f] {
        ret.write_u16::<BigEndian>(*thing).unwrap();
    }
    ret.write_all(&[0, 0]).unwrap();
    ret.write_u16::<BigEndian>(cur_ts).unwrap();
    ret.write_all(&[0, 1, 0, 0, 0, 0, other, 0x0a]).unwrap();
    for _ in ret.len()..512 {
        ret.write_all(&[0]).unwrap();
    }
    assert_eq!(ret.len(), 512);
    ret
}

fn wakeup_loop(sock: UdpSocket, dest: SocketAddr, init: Arc<AtomicBool>) {
    let mut seq_no = 0u16;
    let start = Instant::now();
    loop {
        let cur_ts = Instant::now().duration_since(start).as_millis() as u16;
        let cur_init = init.load(Ordering::Relaxed);
        //println!("heartbeat: seq {}, ts {}, init {}", seq_no, cur_ts, cur_init);
        let hbuf = make_heartbeat(seq_no, cur_ts, cur_init);
        sock.send_to(&hbuf, &dest).unwrap();
        seq_no += 1;
        ::std::thread::sleep_ms(1000);
    }
}

#[test]
fn test_frame_chunker() {
    let test_frame = include_bytes!("../test.mjpeg");
    let mut chonker = FrameChunker::new(0, test_frame as &[u8]);
    let mut test = Vec::new();
    let mut expected_seq = 0;
    while !chonker.is_empty() {
        let buf = chonker.next_chunk();
        test.extend(&buf[4..]);
        assert_eq!(BigEndian::read_u16(&buf[2..]) & 0b0111_1111, expected_seq);
        expected_seq += 1;
    }
    assert_eq!(test_frame as &[u8], &test as &[u8]);
}

#[test]
fn test_frame_chunker_white() {
    let white_framed = include!("../white.rs");
    let mut total_framed = Vec::new();
    let mut total_unframed = Vec::new();
    for buf in white_framed.iter() {
        let mut mut_arr = buf.to_vec();
        BigEndian::write_u16(&mut mut_arr, 0);
        total_framed.extend(&mut_arr);
        total_unframed.extend(&mut_arr[4..]);
    }
    let mut chonker = FrameChunker::new(0, &total_unframed as &[u8]);
    let mut test = Vec::new();
    let mut expected_seq = 0;
    while !chonker.is_empty() {
        let buf = chonker.next_chunk();
        test.extend(&buf);
        assert_eq!(BigEndian::read_u16(&buf[2..]) & 0b0111_1111, expected_seq);
        expected_seq += 1;
    }
    assert_eq!(&total_framed as &[u8], &test as &[u8]);
}

fn spooler_loop(bufs: mpsc::Receiver<Vec<u8>>, tx: Producer<u8>) {
    while let Ok(buf) = bufs.recv() {
        for byte in buf {
            tx.push(byte);
        }
    }
}

#[cfg(any(feature = "tcp", feature = "stdin"))]
fn feeder_loop<R: Read>(mut inp: R, out: mpsc::SyncSender<Vec<u8>>) {
    'outer: loop {
        let mut buf = Vec::with_capacity(BUFFER_SIZE / 2);
        for _ in 0..(BUFFER_SIZE / 2) {
            buf.write_all(&[0]).unwrap();
        }
        let mut read_bytes = 0;
        loop {
            let num = inp.read(&mut buf[read_bytes..]).unwrap();
            if num == 0 {
                eprintln!("EOF!");
                break 'outer;
            }
            read_bytes += num;
            if read_bytes == (BUFFER_SIZE / 2) {
                out.send(buf.clone()).unwrap();
                read_bytes = 0;
            }
        }
    }
}

#[cfg(feature = "stdin")]
fn stdin_source(bufs_tx: mpsc::SyncSender<Vec<u8>>) {
    println!("feed in audio from stdin; ffmpeg format: -f s32be -ar 44100 -ac 2");
    let stdin = io::stdin();
    let lck = stdin.lock();
    let handle = BufReader::new(lck);
    feeder_loop(handle, bufs_tx);
}

#[cfg(feature = "tcp")]
fn tcp_source(bufs_tx: mpsc::SyncSender<Vec<u8>>) {
    let listener = TcpListener::bind("0.0.0.0:1337").unwrap();
    eprintln!("listening on 0.0.0.0:1337; ffmpeg format: -f s32be -ar 44100 -ac 2");
    for stream in listener.incoming() {
        match stream {
            Ok(s) => {
                let btx = bufs_tx.clone();
                std::thread::spawn(move || {
                    feeder_loop(s, btx);
                });
            },
            Err(e) => eprintln!("accept() failed: {}", e)
        }
    }
}

#[cfg(feature = "jack")]
fn jack_source(bufs_tx: mpsc::SyncSender<Vec<u8>>) {
    println!("connecting to JACK");
    let mut conn = JackConnection::connect("showtime-jack", None).unwrap();
    let left = conn.register_port("left", PORT_IS_INPUT).unwrap();
    let right = conn.register_port("right", PORT_IS_INPUT).unwrap();
    let (jack_tx, jack_rx) = make(BUFFER_SIZE * 4);
    let (jack_rs_tx, jack_rs_rx) = mpsc::sync_channel(BUFFER_SIZE);
    let handler = ShowtimeJackHandler {
        left, right,
        tx: jack_tx
    };
    conn.set_handler(handler).unwrap();
    println!("starting JACK feeder");
    Builder::new()
        .name("feeder".into())
        .spawn(move || {
            jack_feeder_loop(jack_rs_rx, bufs_tx);
        })
        .unwrap();
    println!("starting resampler");
    Builder::new()
        .name("resampler".into())
        .spawn(move || {
            resampler_loop(jack_rx, jack_rs_tx);
        })
        .unwrap();
    println!("turning on JACK handler");
    // NOTE: making the conn `_` drops it
    let _conn = match conn.activate() {
        Ok(c) => c,
        Err((_, e)) => Err(e).unwrap()
    };
    println!("sleeping");
    loop {
        ::std::thread::sleep_ms(1000);
    }
}

fn main() {
    let showtime = include_bytes!("../showtime.mjpeg");
    let fuck = include_bytes!("../behind.mjpeg");
    let bork = include_bytes!("../bork.mjpeg");
    println!("*** showtime sender / by eta <https://eta.st/> ***");
    println!("[+] binding UDP sockets (ensure the correct interface has address 192.168.168.55)");
    let vsync = UdpSocket::bind("192.168.168.55:2067").unwrap();
    let vsync_dest: SocketAddr = "226.2.2.2:2067".parse().unwrap();
    let mjpeg = UdpSocket::bind("192.168.168.55:2068").unwrap();
    let mjpeg_dest: SocketAddr = "226.2.2.2:2068".parse().unwrap();
    let sound = UdpSocket::bind("192.168.168.55:2065").unwrap();
    let sound_dest: SocketAddr = "226.2.2.2:2066".parse().unwrap();
    let wakeup = UdpSocket::bind("192.168.168.55:48689").unwrap();
    let wakeup_dest: SocketAddr = "255.255.255.255:48689".parse().unwrap();
    println!("[+] joining multicast groups");
    vsync.join_multicast_v4(&MAGIC_ADDRESS, &"192.168.168.55".parse().unwrap()).unwrap();
    mjpeg.join_multicast_v4(&MAGIC_ADDRESS, &"192.168.168.55".parse().unwrap()).unwrap();
    sound.join_multicast_v4(&MAGIC_ADDRESS, &"192.168.168.55".parse().unwrap()).unwrap();
    println!("[+] setting broadcast option");
    wakeup.set_broadcast(true).unwrap();
    let init = Arc::new(AtomicBool::new(false));
    let fucked = Arc::new(AtomicU8::new(0));
    let ic = init.clone();
    if std::env::var_os("NO_HEARTBEAT").is_none() {
        println!("[+] starting heartbeater");
        Builder::new()
            .name("heartbeater".into())
            .spawn(move || {
                wakeup_loop(wakeup, wakeup_dest, ic);
            })
        .unwrap();
        std::thread::sleep_ms(100);
    }
    init.store(true, Ordering::Relaxed);
    println!("[+] starting audio sender");
    let (tx, rx) = make::<u8>(BUFFER_SIZE);
    let fc = fucked.clone();
    Builder::new()
        .name("audiosender".into())
        .spawn(move || {
            audio_thread(sound, sound_dest, rx, fc);
        })
    .unwrap();
    println!("[+] starting video sender");
    if std::env::var_os("NO_VIDEO").is_none() {
        Builder::new()
            .name("videosender".into())
            .spawn(move || {
                let mut frame_no = 0;
                loop {
                    let image = match fucked.load(Ordering::Relaxed) {
                        // "XRUN - AUDIO PLS" image
                        0 => bork as &[u8],
                        // "AUDIO THREAD BEHIND" image
                        2 => fuck as &[u8],
                        // normal showtime image
                        _ => showtime as &[u8]
                    };
                    let mut chonker = FrameChunker::new(frame_no, image);
                    let vsync_pkt = make_vsync(frame_no);
                    vsync.send_to(&vsync_pkt, &vsync_dest).unwrap();
                    while !chonker.is_empty() {
                        let buf = chonker.next_chunk();
                        mjpeg.send_to(&buf, mjpeg_dest).unwrap();
                    }
                    frame_no += 1;
                    ::std::thread::sleep_ms(33);
                }
            })
        .unwrap();
    }
    println!("[+] starting audio spooler");
    let (bufs_tx, bufs_rx) = mpsc::sync_channel(0);
    Builder::new()
        .name("spooler".into())
        .spawn(move || {
            spooler_loop(bufs_rx, tx);
        })
        .unwrap();
    println!("[+] starting source");
    #[cfg(feature = "tcp")]
    tcp_source(bufs_tx);
    #[cfg(feature = "stdin")]
    stdin_source(bufs_tx);
    #[cfg(feature = "jack")]
    jack_source(bufs_tx);
}

#[cfg(not(any(feature = "tcp", feature = "stdin", feature = "jack")))]
compile_error!("Select an input source by compiling with --features tcp, --features stdin, or --features jack.");
